import { Component, OnInit } from '@angular/core';
import {
  interval,
  timer,
  fromEvent,
  pipe,
  of,
  concat,
  range,
  merge,
  Observable,
  forkJoin,
  Subject,
  ConnectableObservable,
  ReplaySubject,
  BehaviorSubject
} from 'rxjs';
import {
  map,
  filter,
  tap,
  mapTo,
  share,
  take,
  bufferTime,
  switchMap,
  delay,
  concatMap,
  mergeMap,
  scan,
  multicast,
  debounceTime
} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'Rxjs-tut';

  constructor() {}

  public ngOnInit(): void {
    // Ejemplo 1
    // const contador = interval(1000);
    // const trigger = timer(500);
    // contador.subscribe((n) => {
    //   console.log(`Cada ${n} segundos`);
    // });

    // Ejemplo 2
    // trigger.subscribe(n => console.log(n));
    // const mouseClick = fromEvent<MouseEvent>(document, 'click');
    // mouseClick.subscribe((e: MouseEvent) => {
    //   console.log(`Coord: ${e.clientX}, ${e.clientY}`)
    // });

    //Ejemplo 3
    // const nums = of(1,2,3,4,5);
    // const alCuadrado = pipe(
    //   filter((n:number) => n % 2 === 0),
    //   map(n => n*n)
    // );
    // const cuadrado = alCuadrado(nums);
    // cuadrado.subscribe(console.log)

    // Ejemplo 4
    // const clicks = fromEvent<MouseEvent>(document, 'click');
    // const positions = clicks.pipe(
    //   tap(ev => console.log('Position',ev),
    //   err => console.log(err),
    //   () => console.log('complete'))
    // );
    // positions.subscribe(console.log);

    //Ejemplo 5
    // const time = timer(1000);
    // const obs = time.pipe(
    //   tap(() => console.log('Tap On')),
    //   mapTo('End obs')
    // );
    // const sub1 = obs.subscribe(val => console.log(val));
    // const sub2 = obs.subscribe(val => console.log(val));
    // const sharedObs = obs.pipe(share());
    // const sub3 = sharedObs.subscribe(val => console.log(val));
    // const sub4 = sharedObs.subscribe(val => console.log(val));
    //Ejemplo 6
    // const timer = interval(1000).pipe(take(4));
    // const rango = range(1, 10);
    // const result = concat(timer, rango);
    // result.subscribe(console.log)

    // Ejemplo 7
    // const myObs = Observable.create(function (observer: { next: (arg0: number) => void; error: (arg0: string) => void; complete: () => void; }) {
    //   observer.next(1);
    //   observer.next(2);
    //   observer.next(3);
    //   // observer.error('Error Nº 1')
    //   observer.complete();
    // });
    // const subs = myObs.subscribe({
    //   next: (x: any) => console.log(`El siguiente valor es ${x}`),
    //   error: (err: any) => console.log(`Error ${err}`),
    //   complete: () => console.log('Subscripción completa')
    // });
    // subs.unsubscribe();

    // Ejemplo 8
    // const timer = interval(500);
    // const buffer = timer.pipe(bufferTime(2000, 1000));
    // buffer.subscribe(console.log)
    // Ejemplo 9
    // fromEvent(document, 'click').pipe(switchMap(() => interval(1000))).subscribe(console.log)

    // Ejemplo 10
    // const fork = forkJoin(
    //   of('Hola'),
    //   of('Mundo').pipe(delay(500)),
    //   interval(1000).pipe(take(2))
    // );
    // fork.subscribe(val => console.log(val))

    // Ejemplo 11
    // const source = of(2000, 1000, 3000);
    // const osbConcactMap = source.pipe(
    //   concatMap(val => of(`Valor: ${val}`).pipe(delay(val)))
    //   );
    // osbConcactMap.subscribe(console.log)

    // Ejemplo 12
    // const source = of(2000, 1000, 3000);
    // const osbConcactMap = source.pipe(
    //   mergeMap(val => of(`Valor: ${val}`).pipe(delay(val)))
    //   );
    // osbConcactMap.subscribe(console.log)

    // Ejemplo 13
    // const src = of(0,1,2,5,4,5).pipe(delay(500));
    // const scanObs = src.pipe(scan((a:any,c:any) => [...a,c],[]));
    // scanObs.subscribe(console.log)

    // Ejemplo 14
    // const subject = new Subject<any>();
    // subject.subscribe({
    //   next: (n) => console.log(`ObsA: ${n[0]}`)
    // })
    // subject.subscribe({
    //   next: (n) => console.log(`ObsB: ${n[0] + n[1]}`)
    // })
    // subject.next([1, 2]);
    // subject.next([2, 2]);

    // Ejemplo 15
    // const src = of(1,2,3,4,5);
    // const multi = src.pipe(multicast(() => new Subject())) as ConnectableObservable<any>;
    // multi.subscribe({
    //   next: (v) => console.log(`obsA ${v}`)
    // });
    // multi.subscribe({
    //   next: (v) => console.log(`obsB ${v}`)
    // });
    // multi.connect();

    // Ejemplo 16
    // const src = interval(3000).pipe(
    //   tap((n) => console.log(`ID: ${n}`))
    //   );
    // const multi = src.pipe(multicast(() => new Subject)) as ConnectableObservable<any>;
    // multi.subscribe(v => console.log('localhost:4200/',v));
    // multi.subscribe(v => console.log('localhost:4200/',v-1));
    // multi.connect();

    // Ejemplo 17
    // const obs = new ReplaySubject(4);
    // obs.next(1);
    // obs.next(2);
    // obs.next(3);
    // obs.subscribe(console.log);
    // obs.next(4);
    // obs.next(5);
    // obs.subscribe(console.log);

    // Ejemplo 18
    // const subject = new BehaviorSubject(0);
    // const click$ = fromEvent<MouseEvent>(document, 'click').pipe(
    //   map((e: MouseEvent) => ({
    //     x: e.clientX,
    //     y: e.clientY
    //   }))
    // );
    // const interval$ = interval(1000).pipe(
    //   tap(n => subject.next(n))
    // );
    // merge(click$, interval$).subscribe(console.log)


  }
}
